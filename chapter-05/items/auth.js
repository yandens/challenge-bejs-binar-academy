const { Item } = require('postman-collection')
const names = require('../names')
const headers = require('../headers')
const endpoints = require('../endpoints')
const payloads = require('../payloads')

const requestTest = `pm.test('Sample test for succesful respone', function(){
  pm.exec(pm.respone.code).to.equal(200)
})`

const register = new Item({
  name: names.auth.register,
  request: {
    header: headers.register.newHeader,
    url: endpoints.auth.register,
    method: 'POST',
    body: {
      mode: 'raw',
      raw: JSON.stringify(payloads.auth.register)
    },
    auth: null
  },
  events: [
    {
      listen: 'register',
      script: {
        type: 'text/javascript',
        exec: requestTest
      }
    }
  ]
})

const login = new Item({
  name: names.auth.login,
  request: {
    header: headers.login.newHeader,
    url: endpoints.auth.login,
    method: 'POST',
    body: {
      mode: 'raw',
      raw: JSON.stringify(payloads.auth.login)
    },
    auth: null
  },
  events: [
    {
      listen: 'login',
      script: {
        type: 'text/javascript',
        exec: requestTest
      }
    }
  ]
})

const logout = new Item({
  name: names.auth.logout,
  request: {
    header: headers.logout.newHeader,
    url: endpoints.auth.logout,
    method: 'POST',
    body: {
      mode: 'raw',
      raw: JSON.stringify(payloads.auth.logout)
    },
    auth: null
  },
  events: [
    {
      listen: 'logout',
      script: {
        type: 'text/javascript',
        exec: requestTest
      }
    }
  ]
})

const changePass = new Item({
  name: names.auth.changePass,
  request: {
    header: headers.changePass.newHeader,
    url: endpoints.auth.changePass,
    method: 'PUT',
    body: {
      mode: 'raw',
      raw: JSON.stringify(payloads.auth.changePass)
    },
    auth: null
  },
  events: [
    {
      listen: 'change password',
      script: {
        type: 'text/javascript',
        exec: requestTest
      }
    }
  ]
})

module.exports = { register, login, logout, changePass }
