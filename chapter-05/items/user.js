const { Item } = require('postman-collection')
const names = require('../names')
const headers = require('../headers')
const endpoints = require('../endpoints')
const payloads = require('../payloads')

const requestTest = `pm.test('Sample test for succesful respone', function(){
  pm.exec(pm.respone.code).to.equal(200)
})`

const update = new Item({
  name: names.user.update,
  request: {
    header: headers.updateUser.newHeader,
    url: endpoints.user.update,
    method: 'PUT',
    body: {
      mode: 'raw',
      raw: JSON.stringify(payloads.user.update)
    },
    auth: null
  },
  events: [
    {
      listen: 'update user',
      script: {
        type: 'text/javascript',
        exec: requestTest
      }
    }
  ]
})

const destroy = new Item({
  name: names.user.destroy,
  request: {
    header: headers.deleteUser.newHeader,
    url: endpoints.user.destroy,
    method: 'DELETE',
    body: {
      mode: 'raw',
      raw: JSON.stringify(payloads.user.destroy)
    },
    auth: null
  },
  events: [
    {
      listen: 'delete user',
      script: {
        type: 'text/javascript',
        exec: requestTest
      }
    }
  ]
})

module.exports = { update, destroy }
