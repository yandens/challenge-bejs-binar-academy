require('dotenv').config()

const { HOST } = process.env

const register = `${HOST}/auth/register`
const login = `${HOST}/auth/login`
const logout = `${HOST}/auth/logout`
const changePass = `${HOST}/auth/changePass`

module.exports = { register, login, logout, changePass }
