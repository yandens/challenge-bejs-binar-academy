require('dotenv').config

const { HOST } = process.env

const update = `${HOST}/user/update`
const destroy = `${HOST}/user/delete`

module.exports = { update, destroy }
