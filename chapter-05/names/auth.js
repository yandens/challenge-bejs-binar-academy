const register = 'register endpoint'
const login = 'login endpoint'
const logout = 'logout endpoint'
const changePass = 'change password endpoint'

module.exports = { register, login, logout, changePass }
