const register = require('./auth/register')
const login = require('./auth/login')
const logout = require('./auth/logout')
const changePass = require('./auth/changePass')
const updateUser = require('./user/update')
const deleteUser = require('./user/delete')

module.exports = { register, login, logout, changePass, updateUser, deleteUser }
