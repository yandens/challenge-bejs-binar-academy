const register = {
  name: '<your name>',
  username: '<your username>',
  password: '<your password>',
  email: '<your email>',
  phone: '<your phone>'
}

const login = {
  username: '<your username>',
  password: '<your password>'
}

const logout = {}

const changePass = {
  oldPass: '<your old password>',
  newPass: '<your new password>',
  confirmNewPass: '<your new password for confirm>'
}

module.exports = { register, login, logout, changePass }
