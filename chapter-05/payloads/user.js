const update = {
  name: '<your name>',
  username: '<your username>',
  email: '<your email>',
  phone: '<your phone>'
}

const destroy = {}

module.exports = { update, destroy }
