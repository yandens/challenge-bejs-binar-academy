-- DATABASE BENGKEL
-- perintah DDL
-- create
CREATE DATABASE BENGKEL;

-- alter
ALTER DATABASE RENAME TO db_bengkel;

-- drop
DROP DATABASE db_bengkel;


-- TABEL KASIR
-- perintah DDL
-- create
CREATE TABLE kasir (
  id BIGSERIAL PRIMARY KEY,
  nama VARCHAR(255),
  alamat TEXT,
  no_tlp VARCHAR(15)
);
-- alter
ALTER TABLE kasir RENAME TO tb_kasir;
-- drop
DROP TABLE tb_kasir;

-- perintah DML
-- select
SELECT * FROM kasir;
-- insert
INSERT INTO kasir (nama, alamat, no_tlp) VALUES ('kasir1', 'rumah', '081852123456');
-- update
UPDATE kasir SET nama = 'kasir baru' WHERE id = 1;
-- delete
DELETE FROM kasir WHERE id = 1;


-- TABEL COSTUMER
-- perintah DDL
-- create
CREATE TABLE costumer (
  id BIGSERIAL PRIMARY KEY,
  nama VARCHAR(255),
  alamat TEXT,
  no_tlp VARCHAR(15)
);
-- alter
ALTER TABLE costumer RENAME TO tb_costumer;
-- drop
DROP TABLE tb_costumer;

-- perintah DML
-- insert
INSERT INTO costumer (nama, alamat, no_tlp) VALUES ('costumer1', 'rumah', '085123456789');
-- select
SELECT * FROM costumer;
-- update
UPDATE costumer SET nama = 'costumer baru' WHERE id = 1;
-- delete
DELETE FROM costumer WHERE id = 1;


-- TABEL PEMBAYARAN
-- perintah DML
-- create
CREATE TABLE pembayaran (
  id BIGSERIAL PRIMARY KEY,
  costumer_id INT,
  kasir_id INT,
  total_harga INT,
  tanggal DATE,
  FOREIGN KEY (costumer_id) REFERENCES costumer (id),
  FOREIGN KEY (kasir_id) REFERENCES kasir (id)
);
-- alter
ALTER TABLE pembayaran RENAME TO tb_pembayaran;
-- drop
DROP TABLE tb_pembayaran;

-- perintah DDL
-- insert
INSERT INTO pembayaran (costumer_id, kasir_id, total_harga, tanggal) VALUES (1, 1, 500000, '2022-09-09');
-- select
SELECT * FROM pembayaran;
-- update
UPDATE pembayaran SET total_harga = 1000000 WHERE id = 1;
-- delete
DELETE FROM pembayaran WHERE id = 1;


-- TABEL KENDARAAN
-- perintah DDL
-- create
CREATE TABLE kendaraan (
  id BIGSERIAL PRIMARY KEY,
  costumer_id INT,
  nama VARCHAR(255),
  kilometer INT,
  FOREIGN KEY (costumer_id) REFERENCES costumer (id)
);
-- alter
ALTER TABLE kendaraan RENAME TO tb_kendaraan;
-- DROP
DROP TABLE tb_kendaraan;

-- perintah DML
-- insert
INSERT INTO kendaraan (costumer_id, nama, kilometer) VALUES (1, 'costumer1', 45123);
-- select
SELECT * FROM kendaraan;
-- update
UPDATE kendaraan SET kilometer = 50123 WHERE id = 1;
-- delete
DELETE FROM kendaraan WHERE id = 1;


-- TABEL SERVIS
-- perintah DDL
-- create
CREATE TABLE servis (
  id BIGSERIAL PRIMARY KEY,
  kendaraan_id INT,
  tanggal DATE,
  FOREIGN KEY (kendaraan_id) REFERENCES kendaraan (id)
);
-- alter
ALTER TABLE servis RENAME TO tb_servis;
-- drop
DROP TABLE tb_servis;

-- perintah DML
-- insert
INSERT INTO servis (kendaraan_id, tanggal) VALUES (1, '2022-09-09');
-- select
SELECT *  FROM servis;
-- update
UPDATE servis SET tanggal = '2022-09-10' WHERE id = 1;
-- delete
DELETE FROM servis WHERE id = 1;


-- TABEL MEKANIK
-- perintah DDL
-- create
CREATE TABLE mekanik (
  id BIGSERIAL PRIMARY,
  nama VARCHAR(255),
  alamat TEXT,
  no_tlp VARCHAR(15)
);
-- alter
ALTER TABLE mekanik RENAME TO tb_mekanik;
-- drop
DROP TABLE tb_mekanik;

-- perintah DML
-- insert
INSERT INTO mekanik (nama, alamat, no_tlp) VALUES ('mekanik1', 'rumah', '089563214563');
-- select
SELECT * FROM mekanik;
-- update
UPDATE mekanik set nama = 'mekanik baru' WHERE id = 1;
-- delete
DELETE FROM mekanik WHERE id = 1;


-- TABEL SPAREPART
-- perintah DDL
-- create
CREATE TABLE sparepart (
  id BIGSERIAL PRIMARY KEY,
  nama VARCHAR(255),
  stok INT,
  harga_perbuah INT
);
-- alter
ALTER TABLE sparepart RENAME TO tb_sparepart;
-- drop
DROP TABLE tb_sparepart;

-- perintah DML
-- insert
INSERT INTO sparepart (nama, stok, harga_perbuah) VALUES ('sparepart1', 50, 25000);
-- select
SELECT * FROM sparepart;
-- update
UPDATE sparepart SET nama = 'sparepart baru' WHERE id = 1;
-- delete
DELETE FROM sparepart WHERE id = id;


-- TABEL DETAIL SERVIS
-- perintah DDL
-- create
CREATE TABLE detail_servis (
  id BIGSERIAL PRIMARY KEY,
  servis_id INT,
  mekanik_id INT,
  sparepart_id INT,
  jumlah INT,
  FOREIGN KEY (servis_id) REFERENCES servis (id),
  FOREIGN KEY (mekanik_id) REFERENCES mekanik (id),
  FOREIGN KEY (sparepart_id) REFERENCES sparepart (id)
);
-- alter
ALTER TABLE detail_servis RENAME TO tb_detail_servis;
-- drop
DROP TABLE tb_detail_servis;

-- perintah DML
-- insert
INSERT INTO detail_servis (servis_id, mekanik_id, sparepart_id, jumlah) VALUES (1, 1, 1, 5);
-- select
SELECT *  FROM detail_servis;
-- update
UPDATE detail_servis SET jumlah = 3 WHERE id = 1;
-- delete
DELETE FROM detail_servis WHERE id = 1;
