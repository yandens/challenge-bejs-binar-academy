const express = require('express')
const router = express.Router()
const controllers = require('../controllers')
const authorize = require('../helpers/authorize')
const role = require('../utils/role-based/role')
//const middleware = require('../helpers/middleware')

router.post('/register', controllers.auth.register)
router.post('/login', controllers.auth.login)
router.get('/login/google', controllers.auth.loginGoogle)
router.get('/login/facebook', controllers.auth.loginFacebook)
router.post('/logout', authorize(), controllers.auth.logout)
router.put('/change-password', authorize(), controllers.auth.changePass)
router.post('/forgot-password', controllers.auth.forgotPassword)
router.put('/reset-password', controllers.auth.resetPassword)

module.exports = router
