const express = require('express')
const router = express.Router()
const controllers = require('../controllers')
const authorize = require('../helpers/authorize')
const role = require('../utils/role-based/role')

router.post('/create-admin', authorize(role.admin), controllers.admin.createAdmin)
router.get('/users', authorize(role.admin), controllers.admin.showAllUser)
router.get('/user-biodata/:user_id', authorize(role.admin), controllers.admin.showUserBio)
router.get('/user-histories/:user_id', authorize(role.admin), controllers.admin.showUserHistory)
router.get('/user-medias/:user_id', authorize(role.admin), controllers.admin.showUserMedia)
router.delete('/delete-user/:user_id', authorize(role.admin), controllers.admin.deleteUser)
router.put('/acc-payment/:payment_id', authorize(role.admin), controllers.admin.accPayment)

module.exports = router
