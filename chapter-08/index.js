require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const ejs = require('ejs')
const webPush = require('web-push')
const path = require('path')

const app = express()
const router = require('./routes')

app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json())
app.use(morgan('dev'))
app.use(router)

app.use(express.static(path.join(__dirname, 'client')))

app.get('/', (req, res) => {
  res.status(200).json({
    status: true,
    message: 'succes load default page'
  })
})

app.get('/register', (req, res) => {
  res.render('register')
})

webPush.setVapidDetails(
  'mailto:me@email.com',
  process.env.VAPID_PUBLIC,
  process.env.VAPID_PRIVATE,
)

app.post('/welcome', async (req, res, next) => {
  try {
    const subscriptions = req.body
    const payload = JSON.stringify({
      title: 'Welcome',
      body: 'Feel Free to Use Our App',
    })
    const result = await webPush.sendNotification(subscriptions, payload);

    res.status(200).json(result)
  } catch (err) {
    next(err)
  }
})

// 404 handler
app.use((req, res, next) => {
  res.status(404).json({
    status: false,
    message: 'are u lost?'
  })
})

// 500 handler
app.use((err, req, res, next) => {
  //console.log(err)
  res.status(500).json({
    status: false,
    message: err.message
  })
})

/*app.listen(port, () => {
  console.log('listening on http://localhost:3000')
})*/

module.exports = app
