'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.UserBiodata, {
        foreignKey: 'user_id', as: 'biodata'
      })

      User.hasOne(models.UserHistory, {
        foreignKey: 'user_id', as: 'history'
      })

      User.hasMany(models.Media, {
        foreignKey: 'user_id', as: 'media'
      })

      User.hasMany(models.Payment, {
        foreignKey: 'user_id', as: 'payment'
      })
    }
  }
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING,
    user_type: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
