const publicKey = 'BIOJCFkp1KYrQDMx6bCh9ho9keYEwtUQfjA04t6c5557pyFy5fQs-UV5dVLztJKsB6e74cX-9NNz02toPtIW-I0'

async function registerServiceWorker() {
  if ('serviceWorker' in navigator) {
    await Notification.requestPermission()
    if (Notification.permission != 'granted') {
      if (Notification.permission == 'default') {
        registerServiceWorker()
      } else {
        console.error('Notification Denied!')
      }
    } else {
      const register = await navigator.serviceWorker.register('./worker.js', { scope: '/' })

      const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: publicKey
      })

      await fetch('/welcome', {
        method: 'POST',
        body: JSON.stringify(subscription),
        headers: {
          'Content-Type': 'application/json'
        }
      })
    }
  } else {
    console.error('Service workers are not supported in this browser');
  }
}

registerServiceWorker()
