'use strict';
const bcryp = require('bcrypt')
const role = require('../utils/role-based/role')
const userType = require('../utils/oauth2/userType')

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const encrypted = await bcryp.hash('admin', 10)
    await queryInterface.bulkInsert('Users', [{
      username: 'admin',
      password: encrypted,
      role: role.admin,
      user_type: userType.basic,
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {})
  }
};
