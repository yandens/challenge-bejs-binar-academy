const multer = require('multer')
const path = require('path')

const storageImages = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, './public/images')
  },

  // generate unique file name
  filename: (req, file, callback) => {
    const fileName = Date.now() + path.extname(file.originalname)
    callback(null, fileName)
  }
})

const storageVideos = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, './public/videos')
  },

  // generete unique filename
  filename: (req, file, callback) => {
    const namaFile = Date.now() + path.extname(file.originalname);
    callback(null, namaFile)
  }
})

module.exports = { storageImages, storageVideos }
