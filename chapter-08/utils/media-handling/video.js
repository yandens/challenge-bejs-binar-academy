const multer = require('multer')
const storage = require('./storage')

const uploadVideo = multer({
  storage: storage.storageVideos,

  // filter file extension
  fileFilter: (req, file, callback) => {
    if (file.mimetype == 'video/mp4' || file.mimetype == 'video/MPV') {
      callback(null, true);
    } else {
      const err = new Error('only mp4 and mpv allowed to upload!');
      callback(err, false);
    }
  },

  // error handling
  onError: (err, next) => {
    next(err)
  }
})

module.exports = uploadVideo
