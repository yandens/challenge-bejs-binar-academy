const axios = require('axios')
const querystring = require('query-string')
const {
  FACEBOOK_APP_ID,
  FACEBOOK_APP_SECRET,
  FACEBOOK_REDIRECT_URI
} = process.env

const generateAuthURL = () => {
  const params = querystring.stringify({
    client_id: FACEBOOK_APP_ID,
    redirect_uri: FACEBOOK_REDIRECT_URI,
    scope: ['email', 'user_friends'].join(','),
    respon_type: 'code',
    auth_type: 'rerequest',
    display: 'popup'
  })

  return `https://www.facebook.com/v15.0/dialog/oauth?${params}`
}

const getAccessToken = async (code) => {
  const { data } = await axios({
    url: 'https://graph.facebook.com/v15.0/oauth/access_token?',
    method: 'get',
    params: {
      client_id: FACEBOOK_APP_ID,
      redirect_uri: FACEBOOK_REDIRECT_URI,
      client_secret: FACEBOOK_APP_SECRET,
      code
    }
  })

  return data.access_token
}

const getUserInfo = async (accessToken) => {
  const { data } = await axios({
    url: 'https://graph.facebook.com/me',
    method: 'get',
    params: {
      fields: ['id', 'email', 'first_name', 'last_name'].join(','),
      access_token: accessToken,
    }
  })

  return data
}

module.exports = { generateAuthURL, getAccessToken, getUserInfo }
