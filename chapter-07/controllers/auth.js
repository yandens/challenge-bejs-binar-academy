const { User, UserBiodata, UserHistory } = require('../models')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const googleOauth = require('../utils/oauth2/google')
const facebookOauth = require('../utils/oauth2/facebook')
const userType = require('../utils/oauth2/userType')
const role = require('../utils/role-based/role')
const { JWT_KEY } = process.env

const register = async (req, res, next) => {
  try {
    const { name, username, password, email, phone } = req.body

    const existUser = await User.findOne({ where: { username } })

    if (existUser) {
      if (existUser.user_type != userType.basic) {
        return res.status(400).json({
          status: false,
          message: `your account is associated with ${userExist.user_type} oauth\n please login using that!`,
          data: null
        })
      }

      return res.status(400).json({
        status: false,
        message: 'username already exist',
        data: null
      })
    }

    const encryptedPass = await bcrypt.hash(password, 10)

    const user = await User.create({
      username,
      password: encryptedPass,
      role: role.user,
      user_type: userType.basic
    })

    const userBiodata = await UserBiodata.create({
      user_id: user.id,
      name,
      email,
      phone
    })

    return res.status(201).json({
      status: true,
      message: 'success create new user',
      data: {
        name: userBiodata.name,
        username: user.username,
        email: userBiodata.email
      }
    })
  } catch (err) {
    next(err)
  }
}

const login = async (req, res, next) => {
  try {
    const { username, password } = req.body

    const findUser = await User.findOne({ where: { username } })
    const correct = await bcrypt.compare(password, findUser.password)

    if (!findUser || !correct) {
      return res.status(404).json({
        status: false,
        message: 'username or password not correct',
        data: null
      })
    }

    if (findUser.user_type != userType.basic) {
      return res.status(400).json({
        status: false,
        message: `your account is associated with ${userExist.user_type} oauth\n please login using that`,
        data: null
      })
    }

    let date = new Date()

    await UserHistory.create({
      user_id: findUser.id,
      time_login: date,
      time_logout: null
    })

    // JWT_KEY
    const payload = {
      id: findUser.id,
      username: findUser.username,
      role: findUser.role,
      user_type: findUser.user_type
    }
    const token = jwt.sign(payload, JWT_KEY)

    return res.status(200).json({
      status: true,
      message: 'login success',
      data: token
    })
  } catch (err) {
    if (err.message == "Cannot read properties of null (reading 'password')") {
      return res.status(404).json({
        status: false,
        message: 'do you already register?',
        data: null
      })
    }
    next(err)
  }
}

const loginGoogle = async (req, res, next) => {
  try {
    const code = req.query.code
    let payload, date

    if (!code) {
      const url = googleOauth.generateAuthUrl()
      return res.redirect(url)
    }

    // get token
    await googleOauth.setCredentials(code)

    // get user data
    const { data } = await googleOauth.getUserData()

    let userExist = await UserBiodata.findOne({
      where: { email: data.email },
      include: [{
        model: User,
        as: 'user'
      }]
    })

    if (userExist && userExist.user.user_type != userType.google) {
      return res.status(400).json({
        status: false,
        message: `your account is ready in our database, please login using ${userExist.user.user_type} method`,
        data: null
      })
    }

    if (!userExist) {
      userExist = await User.create({
        username: data.name,
        password: null,
        role: role.user,
        user_type: userType.google
      })

      await UserBiodata.create({
        user_id: userExist.id,
        name: data.name,
        email: data.email,
        phone: null
      })

      date = new Date()
      await UserHistory.create({
        user_id: userExist.id,
        time_login: date,
        time_logout: null
      })

      // payload for token
      payload = {
        id: userExist.id,
        username: userExist.username,
        role: userExist.role,
        user_type: userExist.user_type
      }
    } else {
      date = new Date()
      await UserHistory.create({
        user_id: userExist.user.id,
        time_login: date,
        time_logout: null
      })

      // payload for token
      payload = {
        id: userExist.user.id,
        username: userExist.user.username,
        role: userExist.user.role,
        user_type: userExist.user.user_type
      }
    }

    // generate token
    const token = jwt.sign(payload, JWT_KEY)

    return res.status(200).json({
      status: true,
      message: 'login success',
      data: token
    })
  } catch (err) {
    next(err)
  }
}

const loginFacebook = async (req, res, next) => {
  try {
    const code = req.query.code
    let payload, date

    if (!code) {
      const url = facebookOauth.generateAuthURL()
      return res.redirect(url)
    }

    // access token
    const accessToken = await facebookOauth.getAccessToken(code)

    // get user data
    const user = await facebookOauth.getUserInfo(accessToken)
    const username = [user.first_name, user.last_name].join(' ')

    let userExist = await UserBiodata.findOne({
      where: { email: user.email },
      include: [{
        model: User,
        as: 'user'
      }]
    })

    if (userExist && userExist.user.user_type != userType.facebook) {
      return res.status(400).json({
        status: false,
        message: `your account is ready in our database, please login using ${userExist.user.user_type} method`,
        data: null
      })
    }

    if (!userExist) {
      userExist = await User.create({
        username,
        password: null,
        role: role.user,
        user_type: userType.facebook
      })

      await UserBiodata.create({
        user_id: userExist.id,
        name: username,
        email: user.email,
        phone: null
      })

      date = new Date()
      await UserHistory.create({
        user_id: userExist.id,
        time_login: date,
        time_logout: null
      })

      // payload for token
      payload = {
        id: userExist.id,
        username: userExist.username,
        role: userExist.role,
        user_type: userExist.user_type
      }
    } else {
      date = new Date()
      await UserHistory.create({
        user_id: userExist.user.id,
        time_login: date,
        time_logout: null
      })

      // payload for token
      payload = {
        id: userExist.user.id,
        username: userExist.user.username,
        role: userExist.user.role,
        user_type: userExist.user.user_type
      }
    }

    // generate token
    const token = jwt.sign(payload, JWT_KEY)

    return res.status(200).json({
      status: true,
      message: 'login success',
      data: token
    })
  } catch (err) {
    next(err)
  }
}

const logout = async (req, res, next) => {
  try {
    const user = req.user
    let date = new Date()

    await UserHistory.update(
      { time_logout: date },
      { where: { user_id: user.id } }
    )

    const token = jwt.sign(user, " ", { expiresIn: 1 })

    return res.status(200).json({
      status: true,
      message: 'logout success',
      data: token
    })
  } catch (err) {
    next(err)
  }
}

const changePass = async (req, res, next) => {
  try {
    const { oldPass, newPass, confirmNewPass } = req.body
    const user = req.user

    const findUser = await User.findOne({ where: { id: user.id } })
    const correct = await bcrypt.compare(oldPass, findUser.password)

    if (findUser.user_type != userType.basic) {
      return res.status(400).json({
        status: false,
        message: `your account is associated with ${findUser.user_type} oauth\n you don't need password`,
        data: null
      })
    }

    if (!correct || newPass !== confirmNewPass) {
      return res.status(401).json({
        status: false,
        message: 'password not match',
        data: null
      })
    }

    const encryptedPass = await bcrypt.hash(newPass, 10)

    await User.update(
      { password: encryptedPass },
      { where: { id: user.id } }
    )

    return res.status(201).json({
      status: true,
      message: 'success change password',
      data: {
        id: user.id
      }
    })
  } catch (err) {
    next(err)
  }
}

module.exports = { register, login, loginGoogle, loginFacebook, logout, changePass }
