const { User, UserBiodata, UserHistory, Media } = require('../models')
const bcrypt = require('bcrypt')
const role = require('../utils/role-based/role')
const userType = require('../utils/oauth2/userType')

const createAdmin = async (req, res, next) => {
  try {
    const { username, password } = req.body

    const userExist = await User.findOne({ where: { username } })

    if (userExist) {
      return res.status(400).json({
        status: false,
        message: 'username already used',
        data: null
      })
    }

    const encryptedPass = await bcrypt.hash(password, 10)

    const user = await User.create({
      username,
      password: encryptedPass,
      role: role.admin,
      user_type: userType.basic
    })

    return res.status(201).json({
      status: true,
      message: 'success create new admin',
      data: {
        username: user.username,
      }
    })
  } catch (err) {
    next(err)
  }
}

const showAllUser = async (req, res, next) => {
  try {
    const user = await User.findAll()

    res.status(200).json({
      status: true,
      message: 'success get all user data',
      data: user
    })
  } catch (err) {
    next(err)
  }
}

const showUserBio = async (req, res, next) => {
  try {
    const { user_id } = req.params

    if (!user_id) {
      return res.status(400).json({
        status: false,
        message: 'user id not defined!',
        data: null
      })
    }

    const userBio = await User.findOne({
      where: { id: user_id },
      include: [{
        model: UserBiodata,
        as: 'biodata'
      }]
    })

    if (!userBio) {
      return res.status(400).json({
        status: false,
        message: 'user biodata not found!',
        data: null
      })
    }

    return res.status(200).json({
      status: true,
      message: 'success get user biodata',
      data: userBio
    })
  } catch (err) {
    next(err)
  }
}

const showUserHistory = async (req, res, next) => {
  try {
    const { user_id } = req.params

    if (!user_id) {
      return res.status(400).json({
        status: false,
        message: 'user id not defined!',
        data: null
      })
    }

    const userHistory = await UserHistory.findAll({
      where: { user_id },
      include: [{
        model: User,
        as: 'user'
      }]
    })

    if (!userHistory) {
      return res.status(400).json({
        status: false,
        message: 'user histories not found!',
        data: null
      })
    }

    return res.status(200).json({
      status: true,
      message: 'success get user histories',
      data: userHistory
    })
  } catch (err) {
    next(err)
  }
}

const showUserMedia = async (req, res, next) => {
  try {
    const { user_id } = req.params

    if (!user_id) {
      return res.status(400).json({
        status: false,
        message: 'user id not defined!',
        data: null
      })
    }

    const userMedia = await User.findAll({
      where: { id: user_id },
      include: [{
        model: Media,
        as: 'media'
      }]
    })

    if (!userMedia) {
      return res.status(400).json({
        status: false,
        message: 'user media not found!',
        data: null
      })
    }

    return res.status(200).json({
      status: true,
      message: 'success get user medias',
      data: userMedia
    })
  } catch (err) {
    next(err)
  }
}

const deleteUser = async (req, res, next) => {
  try {
    const { user_id } = req.params

    if (!user_id) {
      return res.status(400).json({
        status: false,
        message: 'user id not defined!',
        data: null
      })
    }

    const user = await User.findOne({ where: { id: user_id } })

    if (!user) {
      return res.status(400).json({
        status: false,
        message: 'user not found!',
        data: null
      })
    }

    await Media.destroy({ where: { user_id } })
    await UserHistory.destroy({ where: { user_id } })
    await UserBiodata.destroy({ where: { user_id } })
    await User.destroy({ where: { id: user_id } })

    return res.status(200).json({
      status: true,
      message: 'success delete user account'
    })
  } catch (err) {
    next(err)
  }
}

module.exports = { createAdmin, showAllUser, showUserBio, showUserHistory, showUserMedia, deleteUser }
