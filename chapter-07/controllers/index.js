const auth = require('./auth')
const user = require('./user')
const admin = require('./admin')

module.exports = { auth, user, admin }
