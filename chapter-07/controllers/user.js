const { User, UserBiodata, UserHistory, Media } = require('../models')
const userType = require('../utils/oauth2/userType')

const showBio = async (req, res, next) => {
  try {
    const user = req.user

    const bio = await User.findOne({
      where: { id: user.id },
      include: [{
        model: UserBiodata,
        as: 'biodata',
        attributes: ['name', 'email', 'phone']
      }]
    })

    return res.status(200).json({
      status: true,
      message: 'success get biodata',
      data: bio
    })
  } catch (err) {
    next(err)
  }
}

const updateBio = async (req, res, next) => {
  try {
    const { username, name, email, phone } = req.body
    const user = req.user

    await User.update({ username }, { where: { id: user.id } })

    if (user.user_type != userType.basic) {
      return res.status(400).json({
        status: false,
        message: `you can't change your email because your account is associated with ${user.user_type}`,
        data: null
      })
    }

    await UserBiodata.update({ name, email, phone }, { where: { id: user.id } })

    return res.status(201).json({
      status: true,
      message: 'success update user biodata',
      data: { username, name, email, phone }
    })
  } catch (err) {
    next(err)
  }
}

const showHistory = async (req, res, next) => {
  try {
    const user = req.user

    const history = await UserHistory.findAll({ where: { user_id: user.id } })

    return res.status(200).json({
      status: true,
      message: 'success get user histories',
      data: history
    })
  } catch (err) {
    next(err)
  }
}

const deleteUser = async (req, res, next) => {
  try {
    const user = req.user

    await Media.destroy({ where: { user_id: user.id } })
    await UserHistory.destroy({ where: { user_id: user.id } })
    await UserBiodata.destroy({ where: { user_id: user.id } })
    await User.destroy({ where: { id: user.id } })

    return res.status(200).json({
      status: true,
      message: 'success delete user',
      data: user
    })
  } catch (err) {
    next(err)
  }
}

const uploadImage = async (req, res, next) => {
  try {
    const user = req.user

    if (!req.file) {
      res.status(500).json({
        status: false,
        message: 'file not found!',
        data: null
      })
    }

    const imageUrl = req.protocol + '://' + req.get('host') + '/images/' + req.file.filename
    await Media.create({
      user_id: user.id,
      filename: req.file.filename,
      file_url: imageUrl,
      media_type: 'image'
    })

    return res.status(201).json({
      status: true,
      message: 'success upload image'
    })
  } catch (err) {
    next(err)
  }
}

const uploadVideo = async (req, res, next) => {
  try {
    const user = req.user

    if (!req.file) {
      return res.status(500).json({
        status: false,
        message: 'file not found!',
        data: null
      })
    }

    const videoUrl = req.protocol + '://' + req.get('host') + '/videos/' + req.file.filename
    await Media.create({
      user_id: user.id,
      filename: req.file.filename,
      file_url: videoUrl,
      media_type: 'video'
    })

    return res.status(201).json({
      status: true,
      message: 'success upload video'
    })
  } catch (err) {
    next(err)
  }
}

const updateImage = async (req, res, next) => {
  try {
    const user = req.user

    if (!req.file) {
      res.status(500).json({
        status: false,
        message: 'file not found!',
        data: null
      })
    }

    const imageUrl = req.protocol + '://' + req.get('host') + '/images/' + req.file.filename
    await Media.update(
      {
        filename: req.file.filename,
        file_url: imageUrl,
        media_type: 'image'
      },
      { where: { user_id: user.id } }
    )

    return res.status(201).json({
      status: true,
      message: 'success upload image'
    })
  } catch (err) {
    next(err)
  }
}

const uploadVideos = async (req, res, next) => {
  try {
    const user = req.user

    if (!req.files) {
      return res.status(500).json({
        status: false,
        message: 'file not found!',
        data: null
      })
    }

    req.files.forEach(async (file) => {
      const videoUrl = req.protocol + '://' + req.get('host') + '/videos/' + file.filename
      await Media.create({
        user_id: user.id,
        filename: file.filename,
        file_url: videoUrl,
        media_type: 'video'
      })
    })

    return res.status(201).json({
      status: true,
      message: 'success upload video'
    })
  } catch (err) {
    next(err)
  }
}

module.exports = { updateBio, deleteUser, showBio, showHistory, uploadImage, updateImage, uploadVideo, uploadVideos }
