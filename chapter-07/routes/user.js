const express = require('express')
const router = express.Router()
const controllers = require('../controllers')
const authorize = require('../helpers/authorize')
const role = require('../utils/role-based/role')
const image = require('../utils/media-handling/image')
const video = require('../utils/media-handling/video')

router.get('/biodata', authorize(role.user), controllers.user.showBio)
router.post('/upload-image', authorize(role.user), image.single('image'), controllers.user.uploadImage)
router.post('/update-image', authorize(role.user), image.single('image'), controllers.user.uploadImage)
router.post('/upload-video', authorize(role.user), video.single('video'), controllers.user.uploadVideo)
router.post('/upload-videos', authorize(role.user), video.array('videos'), controllers.user.uploadVideos)
router.put('/update', authorize(role.user), controllers.user.updateBio)
router.get('/histories', authorize(role.user), controllers.user.showHistory)
router.delete('/delete', authorize(role.user), controllers.user.deleteUser)

module.exports = router
