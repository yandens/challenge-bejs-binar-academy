const tambah = (a, b) => a + b;
const kurang = (a, b) => a - b;
const kali = (a, b) => a * b;
const bagi = (a, b) => a / b;
const akarKuadrat = (a) => Math.sqrt(a);
const luasPersegi = (s) => s**2;
const volumeKubus = (s) => s**3;
const volumeTabung = (r, t) => 3.14 * r**2 * t;
const readline = require('readline').createInterface({
    input : process.stdin,
    output : process.stdout
});

function input(question){
    return new Promise(resolve => {
        readline.question(question, data => {
            return resolve(data);
        });
    });
}

async function validasiMinus(a){
    let valid = false;

    try {
        if(a > 0){
            valid = true;
        }
    
        while(!valid){
            a = await input("Ulangi, inputan tidak valid       : ");
            if(a > 0){
                valid = true;
            }
        }
        
        return a;
    } catch (error) {
        console.log(error);
    }
}

async function validasiHuruf(a){
    let valid = false;
    
    try {
        if(a > 0 || a < 0){
            valid = true;
        }
    
        while(!valid){
            a = await input("Ulangi, inputan tidak valid       : ");
            if(a > 0 || a < 0){
                valid = true;
            }
        }
    
        return a;
    } catch (error) {
        console.log(error);
    }
}

async function kalkulasi(pilihan){
    let hasil;

    try {
        if(pilihan > 0 && pilihan <= 8 ){
            if(pilihan >= 1 && pilihan <= 4){
                let a = await input("Masukkan angka pertama            : ");
                a = await validasiHuruf(a);
                let b = await input("Masukkan angka kedua              : ");
                b = await validasiHuruf(b);
                readline.close();
                if(pilihan == 1){
                    hasil = tambah(+a, +b);
                }else if(pilihan == 2){
                    hasil = kurang(+a, +b);
                }else if(pilihan == 3){
                    hasil = kali(+a, +b);
                }else{
                    hasil = bagi(+a, +b);
                }
            }else if(pilihan == 5){
                let a = await input("Masukkan angka                    : ");
                a = await validasiMinus(a);
                readline.close();
                hasil = akarKuadrat(+a);
            }else if(pilihan >= 6 && pilihan <= 7){
                let s = await input("Masukkan panjang sisi             : ");
                s = await validasiMinus(s);
                readline.close();
                if(pilihan == 6){
                    hasil = luasPersegi(+s);
                }else{
                    hasil = volumeKubus(+s);
                }
            }else{
                let r = await input("Masukkan jari-jari                : ");
                r = await validasiMinus(r);
                let t = await input("Masukkan tinggi                   : ");
                t = await validasiMinus(t);
                readline.close();
                hasil = volumeTabung(+r, +t);
            }
    
            console.log("========================================");
            console.log(`Hasil                             : ${hasil}`);
        }else{
            console.clear();
            console.log("Maaf pilihan menu tidak tersedia! Ulangi!");
            main();
        }
    } catch (error) {
        console.log(error);
    }
}

function menu(){
    console.log("========================================");
    console.log("               CHALLANGE-01             ");
    console.log("       PROGRAM KALKULASI SEDERHANA      ");
    console.log("========================================");
    console.log(" 1. Tambah             5. Akar Kuadrat");
    console.log(" 2. Kurang             6. Luas Persegi");
    console.log(" 3. Kali               7. Volume Kubus");
    console.log(" 4. Bagi               8. Volume Tabung");
    console.log("========================================");
}

async function main(){
    menu();
    let pilihan = await input("Masukkan pilihan menu             : ");
    kalkulasi(pilihan);
}

main();